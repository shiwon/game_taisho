#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "Rooftop.h"

static int ka;

void Rooftop_Initialize() {
	ka= LoadGraph("画像/Game/006.jpg");
}

void Rooftop_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Over);//シーンをタイトル画面に変更
	}
}

void Rooftop_Draw() {
	DrawGraph(0, 0, ka, TRUE);
}

void Rooftop_Finalize() {

}