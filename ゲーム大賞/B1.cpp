#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "B1.h"

static int ka;

void B1_Initialize() {
	ka= LoadGraph("画像/Game/006.jpg");
}

void B1_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Over);//シーンをタイトル画面に変更
	}
}

void B1_Draw() {
	DrawGraph(0, 0, ka, TRUE);
}

void B1_Finalize() {

}