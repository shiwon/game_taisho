#include "DxLib.h"
#include "SceneMgr.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	ChangeWindowMode(TRUE), SetGraphMode(1280, 768, 32), DxLib_Init(), SetDrawScreen(DX_SCREEN_BACK); //ウィンドウモード変更,ウィンドウサイズ変更,初期化,裏画面設定

	SceneMgr_Initialize();   //初期化

	while (ScreenFlip() == 0 && ProcessMessage() == 0 && ClearDrawScreen() == 0) {

		SceneMgr_Update();//更新
		SceneMgr_Draw();//描画


	}

	SceneMgr_Finalize();   //終了

	DxLib_End();
	return 0;
}