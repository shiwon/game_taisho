#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "Title.h"

static int Title_haikei;
static int Title_logo;
static int Title_bgm;

void Title_Initialize() {
	Title_haikei = LoadGraph("画像/Title/018b.jpg");
	Title_logo = LoadGraph("画像/Title/moguru.png");
}

void Title_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Forest);//シーンをタイトル画面に変更
	}
}

void Title_Draw() {
	DrawRotaGraph(0, 0, 2.5, 0, Title_haikei, TRUE);
	DrawGraph(270, 340, Title_logo, TRUE);
	DrawFormatString(200, 600, GetColor(0, 0, 0), "Space to Start the Game.");
}

void Title_Finalize() {
	
}
