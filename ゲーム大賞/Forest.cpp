#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "Forest.h"

static int ka;

void Forest_Initialize() {
	ka= LoadGraph("画像/Game/006.jpg");
}

void Forest_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Over);//シーンをタイトル画面に変更
	}
}

void Forest_Draw() {
	DrawGraph(0, 0, ka, TRUE);
}

void Forest_Finalize() {

}