#ifndef DEF_FOREST_H

#define DEF_FOREST_H

void Forest_Initialize();//初期化

void Forest_Update();//更新

void Forest_Draw();//描画

void Forest_Finalize();//終了処理

#endif 