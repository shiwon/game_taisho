#include "DxLib.h"
#include "SceneMgr.h"

#include "Title.h"
#include "Forest.h"
#include "F1.h"
#include "B1.h"
#include "B2.h"
#include "B3.h"
#include "B4.h"
#include "Rooftop.h"

#include "Over.h"

static eScene mScene = eScene_Title;       //シーン管理変数
static eScene mNextScene = eScene_None;    //次のシーン管理変数

static void SceneMgr_InitializeModule(eScene scene);//指定モジュールを初期化する
static void SceneMgr_FinalizeModule(eScene scene);//指定モジュールの終了処理を行う

void SceneMgr_Initialize() {   //初期化
	SceneMgr_InitializeModule(mScene);
}

void SceneMgr_Update() {     //更新
	if (mNextScene != eScene_None) {    //次のシーンがセットされていたら
		SceneMgr_FinalizeModule(mScene);//現在のシーンの終了処理を実行
		mScene = mNextScene;    //次のシーンを現在のシーンセット
		mNextScene = eScene_None;    //次のシーン情報をクリア
		SceneMgr_InitializeModule(mScene);    //現在のシーンを初期化
	}
	switch (mScene) {       //シーンによって処理を分岐
	case eScene_Title:    //現在の画面がメニューなら
		Title_Update();   //メニュー画面の更新処理をする
		break;//以下略
	case eScene_Forest:
		Forest_Update();
		break;
	case eScene_F1:
		F1_Update();
		break;
	case eScene_B1:
		B1_Update();
		break;
	case eScene_B2:
		B2_Update();
		break;
	case eScene_B3:
		B3_Update();
		break;
	case eScene_B4:
		B4_Update();
		break;
	case eScene_Rooftop:
		Rooftop_Update();
		break;


	case eScene_Over:
		Over_Update();
		break;
	}
}

void SceneMgr_Draw() {       //描画

	switch (mScene) {      //シーンによって処理を分岐
	case eScene_Title:   //現在の画面がメニュー画面なら
		Title_Draw();    //メニュー画面の描画処理をする
		break;//以下略
	case eScene_Forest:
		Forest_Draw();
		break;
	case eScene_F1:
		F1_Draw();
		break;
	case eScene_B1:
		B1_Draw();
		break;
	case eScene_B2:
		B2_Draw();
		break;
	case eScene_B3:
		B3_Draw();
		break;
	case eScene_B4:
		B4_Draw();
		break;
	case eScene_Rooftop:
		Rooftop_Draw();
		break;


	case eScene_Over:
		Over_Draw();
		break;
	}
}


// 引数 nextScene にシーンを変更するcha
void SceneMgr_ChangeScene(eScene NextScene) {
	mNextScene = NextScene;    //次のシーンをセットする
}


// 引数sceneモジュールを初期化する
static void SceneMgr_InitializeModule(eScene scene) {
	switch (scene) {          //シーンによって処理を分岐
	case eScene_Title:       //指定画面がメニュー画面なら
		Title_Initialize();  //メニュー画面の初期化処理をする
		break;//以下略
	case eScene_Forest:
		Forest_Initialize();
		break;
	case eScene_F1:
		F1_Initialize();
		break;
	case eScene_B1:
		B1_Initialize();
		break;
	case eScene_B2:
		B2_Initialize();
		break;
	case eScene_B3:
		B3_Initialize();
		break;
	case eScene_B4:
		B4_Initialize();
		break;
	case eScene_Rooftop:
		Rooftop_Initialize();
		break;


	case eScene_Over:
		Over_Initialize();
		break;
	}
}

// 引数sceneモジュールの終了処理を行う
static void SceneMgr_FinalizeModule(eScene scene) {
	switch (scene) {         //シーンによって処理を分岐
	case eScene_Title:      //指定画面がメニュー画面なら
		Title_Finalize();   //メニュー画面の終了処理処理をする
		break;//以下略
	case eScene_Forest:
		Forest_Finalize();
		break;
	case eScene_F1:
		F1_Finalize();
		break;
	case eScene_B1:
		B1_Finalize();
		break;
	case eScene_B2:
		B2_Finalize();
		break;
	case eScene_B3:
		B3_Finalize();
		break;
	case eScene_B4:
		B4_Finalize();
		break;
	case eScene_Rooftop:
		Rooftop_Finalize();
		break;


	case eScene_Over:
		Over_Finalize();
		break;
	}
}

void SceneMgr_Finalize() {   //終了
	SceneMgr_InitializeModule(mScene);
}
