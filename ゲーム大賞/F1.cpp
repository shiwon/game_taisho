#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "F1.h"

static int ka;

void F1_Initialize() {
	ka= LoadGraph("画像/Game/006.jpg");
}

void F1_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Over);//シーンをタイトル画面に変更
	}
}

void F1_Draw() {
	DrawGraph(0, 0, ka, TRUE);
}

void F1_Finalize() {

}