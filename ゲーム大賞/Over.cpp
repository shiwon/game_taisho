#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "Over.h"

static int Over_haikei;
static int Over_logo;
static int Over_bgm;
static int EndScore;

void Over_Initialize() {
	Over_haikei = LoadGraph("画像/GameOver/Over.jpg");
	Over_logo = LoadGraph("画像/GameOver/Over_logo.png");
	
}

void Over_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Title);//シーンをタイトル画面に変更
	}
}




void Over_Draw() {
	DrawRotaGraph(0, 0, 2.5, 0, Over_haikei, TRUE);
	DrawGraph(270, 340, Over_logo, TRUE);
	DrawFormatString(200, 600, GetColor(255, 255, 255), "Space to return to title");
	
}

void Over_Finalize() {

}