#ifndef DEF_SCENEMGR_H

#define DEF_SCENEMGR_H


typedef enum {
	eScene_Title,        //タイトル画面
	eScene_Forest,         //ゲーム画面
	eScene_F1,
	eScene_B1,
	eScene_B2,
	eScene_B3,
	eScene_B4,
	eScene_Rooftop,

	eScene_Over,     //ゲームオーバー画面

	eScene_None,
}eScene;

//void MorF(int a);
//int MorF_t();


void SceneMgr_Initialize();//初期化

void SceneMgr_Update();//更新

void SceneMgr_Draw();//描画

void SceneMgr_Finalize();//終了処理

// 引数 nextScene にシーンを変更する
void SceneMgr_ChangeScene(eScene nextScene);
#endif 