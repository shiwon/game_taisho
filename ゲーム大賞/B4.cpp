#include "DxLib.h"
#include "SceneMgr.h"
#include "Keyboard.h"
#include "B4.h"

static int ka;

void B4_Initialize() {
	ka= LoadGraph("画像/Game/006.jpg");
}

void B4_Update() {
	Keyboard_Update();
	if (Keyboard_Get(KEY_INPUT_SPACE) == 1) {
		SceneMgr_ChangeScene(eScene_Over);//シーンをタイトル画面に変更
	}
}

void B4_Draw() {
	DrawGraph(0, 0, ka, TRUE);
}

void B4_Finalize() {

}